# VIS

“vi-simple” a simple text editor with vi-like user interface.
This is purely educational and recreational, do not expect production level code.

## Usage
```console
$ make
$ ./vis <file>
```

## Current State

Currently the program functions like a [pager](https://en.wikipedia.org/wiki/Terminal_pager)
with movement on the x axis and line counts.
Moving after the end of the file causes weird behaviours.
