#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include <ncurses.h>

#define UNUSED(x) (void)x
#define UNIMPLEMENTED                                                          \
  do {                                                                         \
    fprintf(stderr, "%s:%d: ERROR: function '%s' is not implemented\n",        \
            __FILE__, __LINE__, __func__);                                     \
    exit(1);                                                                   \
  } while (0)

#define NEWLINE_BUFFER_CAP 32768
size_t newline_buffer[NEWLINE_BUFFER_CAP] = {0};
size_t newline_buffer_len = 0;

#define TEXT_BUFFER_CAP 512000
char text_buffer[TEXT_BUFFER_CAP] = {0};

void newline_buffer_dump(FILE *stream)
{
  for (size_t i = 0; i < newline_buffer_len; ++i) {
    fprintf(stream, "%lu, ", newline_buffer[i]);
  }
}

void newline_buffer_populate(const char *text_buffer)
{
  for (size_t i = 0; i < TEXT_BUFFER_CAP; ++i) {
    if (text_buffer[i] == '\n')
      newline_buffer[newline_buffer_len++] = i;
    if (!text_buffer[i] || text_buffer[i] == EOF)
      break;
  }
}

size_t newline_buffer_line_start(const size_t line_index)
{
  assert(line_index <= newline_buffer_len + 1);

  if (line_index == 0) {
    return 0;
  } else {
    return newline_buffer[line_index - 1] + 1;
  }
}

// Return line lenght including newline.
size_t newline_buffer_line_len(const size_t line_index)
{
  assert(line_index <= newline_buffer_len);

  if (line_index == 0) {
    return newline_buffer[0];
  } else {
    return newline_buffer[line_index] - newline_buffer[line_index - 1];
  }
}

void print_partial(FILE *stream, char *str, size_t len, size_t begin,
                   size_t end)
{
  assert(begin < len);
  assert(end < len);
  printf("%lu", end);
  for (size_t i = begin; i < end; ++i) {
    fprintf(stream, "%c", str[i]);
  }
}

#define printw_partial(str, len, begin, end)                                   \
  wprintw_partial(stdscr, str, len, begin, end)
void wprintw_partial(WINDOW *win, char *str, size_t len, size_t begin,
                     size_t end)
{
  assert(begin < len);
  assert(end < len);
  for (size_t i = begin; i < end; ++i) {
    wprintw(win, "%c", str[i]);
  }
}

// at least I hope
#define typesafe(x) (__typeof__(x))x
#define max(x, y) typesafe(x) > typesafe(y) ? x : y
#define min(x, y) typesafe(x) < typesafe(y) ? x : y

size_t clamp(const size_t min_v, const size_t v, const size_t max_v)
{
  size_t tmp = max(v, min_v);
  return min(tmp, max_v);
}

// Stolen from <https://github.com/tsoding>
const char *shift(int *argc, char ***argv)
{
  assert(*argc > 0);
  const char *result = **argv;
  *argc -= 1;
  *argv += 1;
  return result;
}

void usage(FILE *stream, const char *program_name)
{
  fprintf(stream, "Usage: %s <file>\n", program_name);
}

typedef enum { PAIR_DEFAULT = 1, PAIR_SELECTED } Pairs;

// setup and initialization of ncurses screen
void ncurses_init()
{
  initscr();
  start_color();
  noecho();
  nodelay(stdscr, 1);
  refresh();
}

// final cleanup and end of ncurses screen
void ncurses_end() { endwin(); }

typedef struct {
  size_t x, y, line;
  bool updated;
} Cursor;

// TODO: fix weird behaviour after EOF.
// TODO: better x movement.
// TODO: editing.
int main(int argc, char *argv[])
{
  const char *program_name = shift(&argc, &argv);
  if (argc == 0) {
    usage(stderr, program_name);
    fprintf(stderr, "ERROR: no input file is given.\n");
    exit(1);
  }

  FILE *fd = fopen(shift(&argc, &argv), "r");
  fread(text_buffer, sizeof(text_buffer), 1, fd);
  newline_buffer_populate(text_buffer);
  newline_buffer_dump(stdout);

  Cursor cursor = {.x = 0, .y = 0, .line = 0, .updated = TRUE};
  char c;
  ncurses_init();
  while ((c = getch()) != 'q') {
    switch (c) {
    case 'j':
      ++cursor.y;
      if (cursor.y >= (size_t) LINES) {
        --cursor.y;
        ++cursor.line;
      }
      cursor.updated = TRUE;
      break;
    case 'k':
      if (cursor.y == 0) {
        cursor.line = (cursor.line == 0 ? 0 : cursor.line - 1);
      } else {
        --cursor.y;
      }
      cursor.updated = TRUE;
      break;
    case 'h':
      cursor.x = (cursor.x == 0 ? 0 : cursor.x - 1);
      cursor.updated = TRUE;
      break;
    case 'l':
      ++cursor.x;
      cursor.updated = TRUE;
      break;
    }

    if (cursor.updated) {
      clear();
      for (int i = 0; i < LINES && i + cursor.line <= newline_buffer_len; ++i) {
        printw("%4zu| ", cursor.line + i);
        printw_partial(text_buffer,
                       sizeof(text_buffer) / sizeof(text_buffer[0]),
                       newline_buffer_line_start(cursor.line + i),
                       newline_buffer_line_start(cursor.line + i + 1));
      }
      move(cursor.y,
           6 + clamp(0, cursor.x, newline_buffer_line_len(cursor.line)));
      refresh();
      cursor.updated = FALSE;
    }
  }
  ncurses_end();
}
