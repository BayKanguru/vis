CC = gcc
CFLAGS = -Wall -Wextra -Werror -std=c11 -pedantic -ggdb
LIBS = ncurses
LDFLAGS += $(addprefix -l, $(LIBS))

vis: vis.c
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

clean:
	rm vis
